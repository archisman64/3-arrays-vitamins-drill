const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

//      1       //
function getAvailableItems(itemsList) {
    // returns those items that have the availabile parameter set as true
    const availableItems = itemsList.filter((item) => item.available === true);
    return availableItems;
}
console.log('List of available items:' ,getAvailableItems(items));

//      2       //
function getItemsWithVitaminC(itemsList) {
    const itemsWithVitaminC = itemsList.filter((item) => item.contains.includes('Vitamin C'))
    return itemsWithVitaminC;
}
console.log('Items with vitamin C:', getItemsWithVitaminC(items));

//      3       //
function getItemsWithVitaminA(itemsList) {
    const itemsWithVitaminA = itemsList.filter((item) => item.contains.includes('Vitamin A'))
    return itemsWithVitaminA;
}
console.log('Items with vitamin A:', getItemsWithVitaminA(items));

//      4       //
function groupItems(itemsList) {
    const result = {};
    itemsList.map((item) => {
        const fruitName = item.name;
        const vitamins = item.contains.split(',');

        vitamins.map((vitamin) => {
            if(!result[vitamin.trim()]) {
                result[vitamin.trim()] = [];
            }
            result[vitamin.trim()].push(fruitName);
        })
    });
    return result;
}
console.log('List of items after grouping:', groupItems(items));

//      5       //
function sortItems(itemsList) {
    const newList = itemsList.map((item) => {
        // counting the number of vitamins for each item
        const contentsLenghth = item.contains.split(',').length;
        // creating a new copy of that item with an additional 'length' feild
        const itemWithLength = {...item, 'length': contentsLenghth};
        return itemWithLength;
    })
    return newList.sort((a,b) => b.length - a.length);
}
console.log('List of items sorted according to number of vitamins', sortItems(items));